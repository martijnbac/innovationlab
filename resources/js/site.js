// This is all you.
$( "#loginForm" ).submit(function( event ) {
  event.preventDefault();
  $(this).find('button').text('Logging in...').addClass('loading').attr('disabled','disabled');
  var data = $('form input').serializeArray();
  console.log("data",data);
  const userData = {
    username: data[0].value,
    password: data[1].value,
    autoLogin: data[2] && data[2].value == 'on' ? true : false,
  };

  if(userData.username == "" || userData.password == "") {
    return validationMessage("All fields are required.");
  } 

  login(userData);
 
  function validationMessage(error) {
    if (error) {
      $('.form-group--login button').text('Log in').removeClass('loading').removeAttr('disabled','disabled');


      $('.alert').addClass('alert-error').html(error);

    } else {
      // this.setState({ redirect: true });
    }
  }
});


function login(userData) {
  // based on React app -> auth.js
  axios
  .post("https://api-staging.innovationlab.nhlstenden.com/login_check", userData)
  .then((response) => {
    const token = response.data.token;
    const refresh_token = response.data.refresh_token;

    // store token in localStorage
    // this.storeToken(token);
    localStorage.token = token;

    const autoLogin = false;//userData.autoLogin;
    // this.storeAutoLogin(autoLogin);

    if (autoLogin) {
      // if autologin, store refresh token in localStorage
      // store boolean
      this.storeRefreshToken(refresh_token);
    } else {
      // else, remove refresh token?
      localStorage.removeItem("refresh_token");
    }

    // if refresh token, request new token

    // create instance
    // this.createInstance();

    // callback
    // if (cb) cb();
    window.location.replace("https://innovationlab.nhlstenden.com/");
  })
  .catch(function (error) {
    // callback
    if (cb) cb(error);
    // window.location.replace("http://localhost:3000/");

  });

  function cb(error) {
    if (error) {
      $('.form-group--login button').text('Log in').removeClass('loading').removeAttr('disabled','disabled');


      $('.alert').addClass('alert-error').html(error.response.data.message);

    } else {
      // this.setState({ redirect: true });
    }
  }
  
}

const bardSwiper = new Swiper(".bard-swiper", {
  // Optional parameters
  slidesPerView: 'auto',
  spaceBetween: 24,
  navigation: {
      nextEl: ".swiper__button-next",
      prevEl: ".swiper__button-prev",
  }
});


const swiper = new Swiper(".section-swiper", {
    // Optional parameters
    slidesPerView: 1.5,
    spaceBetween: 24,
    navigation: {
        nextEl: ".swiper__button-next",
        prevEl: ".swiper__button-prev",
    },
    breakpoints: {

        1024: {
          slidesPerView: 3
        }
    
      },
});



// $('.header__nav a.hash').on('click', function(e) {
//   var hash = $(this).attr("href");
//   console.log(window.location, hash);
//   var path = window.location.pathname;
//   console.log(path);

//   // only animate on homepage
//   if(path === "/nl" || path === "/en") {
//     e.preventDefault();
//     let h = hash.substr(1);
//     console.log(h);
//    $('html,body').animate({scrollTop: $(h).offset().top},'slow');

//   }
// })