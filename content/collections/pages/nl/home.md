---
id: 367c0c11-b2fc-4ef0-a54d-501f320bfca3
blueprint: home
title: InnovationLab
description: 'Ondersteuning en voorlichting voor innovatief onderwijs!'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651008064
secondary_title: 'Online platform'
secondary_description: 'Innovatieve projecten en internationale samenwerking'
image: 'Academie-VO-&-MBO-38-b.jpg'
template: home
---
