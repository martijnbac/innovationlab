---
id: 9a27bf1c-3d6c-44f1-a032-1969bf776904
blueprint: pages
title: Privacyverklaring
template: default
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1654262537
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Last updated: 08 October 2021'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Welcome to InnovationLab! InnovationLab is a free, non-commercial network, developed by NHL Stenden University in the Netherlands. The main objective is : Fostering cocreative learning and communication across the globe.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'At InnovationLab we believe that privacy is important and we know that you care about how your personal information is dealt with. That is why we have created the InnovationLab Privacy Promises as described in this document.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'By accessing or using our platform you acknowledge that you accept the practices and policies outlined in this privacy policy.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This privacy policy contains detailed information about how personal information is dealt with. The privacy policy may be amended at any time, with or without notice to users. We encourage you to periodically visit this page to review the current privacy policy.'
  -
    type: paragraph
    content:
      -
        type: text
        text: "If you have any questions or concerns regarding our privacy policy, please send a detailed message to\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'mailto:innovationlab@nhlstenden.com'
              rel: null
              target: null
              title: null
        text: innovationlab@nhlstenden.com
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'The InnovationLab Privacy Promises'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Your data is yours'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Your profile is only visible to other InnovationLab members'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We do not sell or share your data with other parties'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Your data is securely stored on our university servers'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Teacher & parent/guardian consent'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'InnovationLab requires a parent or guardian to provide consent before a user account can be created for a child under the age of 18. Teachers and school administrators may provide consent on behalf of parents.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'New user accounts for students are created on request by a teacher or school administrator to ensure a safe learning environment where everyone is who they say they are. As a teacher or school administrator, it is your responsibility to confirm that you have authority to consent on behalf of your student’s parents.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Consent may be revoked at any time.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Account & profile information'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'In order to provide our service and to create a user account we need the following information:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Your name'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The country you live in'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The school you attend or work for'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We usually ask for an e-mail address as well. Although connecting an e-mail address to your account is strongly recommended, it is not compulsory. When an e-mail address is connected to your account it is used as your username and it allows you to reset your password yourself. It may also be used to contact you for a number of reasons, including, but not limited to, informing you of any changes to our service and anything related to your user account.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'All users have a user profile to enable them to use the platform. Your profile displays your name, the country you live in and the school you attend or work at by default. It may also display whether you are a student, student teacher or teacher. This information is displayed as long as you remain a member of the network and can only be changed by InnovationLab moderators.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'You are free to add other information to your user profile, such as, but not limited to, your birthday, gender, hobbies, interests and pictures.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Messages, Events, Projects & Dilemmas'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The information, pictures and/or documents you provide in (Wall/profile) messages, Events, Projects, Dilemmas and/or on School pages are securely stored on our university servers. You provide this infomation voluntarily and you can delete it at any time.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Usage data'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'General usage data is used for research purposes and to help improve the performance of the platform'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Usage Data may include information such as your device''s Internet Protocol address, browser type, browser version, the pages of our website that you visit, the time and date of your visit, the time spent on those pages, and other diagnostic data.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Use of your personal data'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'InnovationLab may use personal data for the following purposes:'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'To provide and maintain our service, including to monitor the usage of our service.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'To manage your account: to manage your registration as a user of the platform. The personal data you provide can give you access to different functionalities of the website that are available to you as a registered user.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'To contact you: to contact you by e-mail regarding updates or informative communications related to the functionalities, Wall/profile messages, Events or other features, including security updates, when necessary or reasonable for their implementation.'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'To manage your requests: to attend and manage your requests to us.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Data ownership & deleting your account'
  -
    type: paragraph
    content:
      -
        type: text
        text: "The InnovationLab user is the owner of any data and personal information submitted through InnovationLab. As a user, you can delete any information you provided at any time except your name, the country you live in and the school you attend or work for. Your name, the country you live in and the school you attend or work at can be changed on request by a moderator. If you wish to change the name, country or school that is displayed on your profile you can send an e-mail to\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'mailto:innovationlab@nhlstenden.com'
              rel: null
              target: null
              title: null
        text: innovationlab@nhlstenden.com
      -
        type: text
        text: "\_with your request. Make sure you use the e-mail address your account was registered with, or mention your username if applicable, so we can verify that the request belongs to you."
  -
    type: paragraph
    content:
      -
        type: text
        text: 'At InnovationLab, we believe in lifelong learning and that having access to the platform benefits learners of all levels and ages. Because of this we do not delete or anonymise user accounts by default. However, a user account can be deleted on request by the user or their teacher, a student’s parents or legal guardian at any time by contacting innovationlab@nhlstenden.com. Please include the user’s username and email address (when applicable) so that we can better comply with your request.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Security & storage of your data'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Your user account and the data you provide us with is stored on our university servers in the Netherlands.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'When you use our platform, you are consenting to have your data transferred to and processed in the Netherlands.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: Cookies
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The platform uses functional cookies, for example to remember choices you make to give you better functionality and personal features. We don''t use Analytical cookies or tracking cookies.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Links to other websites'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Our service may contain links to other websites, posted by us or our users, that are not operated by us. If you click on a third-party link, you will be directed to that third party''s site. We strongly advise you to review the Privacy Policy of every site you visit. We have no control over and assume no responsibility for the content, privacy policies or practices of any third-party sites or services.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: Disclaimer
  -
    type: paragraph
    content:
      -
        type: text
        text: 'While we have taken all reasonable steps to limit the uses of information you provide and we may allow you to set privacy options that limit access to information you post on InnovationLab, please be aware that no security measures are perfect or impenetrable. We cannot and do not guarantee that this information will remain permanently secure and will not be viewed by unauthorized people.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'InnovationLab, as part of NHL Stenden University, promises their best efforts to keep your data secure and intact. However, there is always a small possibility that your information could be lost due to technical problems. We are not responsible for any such loss.'
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'More information'
  -
    type: paragraph
    content:
      -
        type: text
        text: "If you have any questions about our privacy policy, please feel free to contact us at\_"
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'mailto:innovationlab@nhlstenden.com'
              rel: null
              target: null
              title: null
        text: innovationlab@nhlstenden.com
  -
    type: paragraph
    content:
      -
        type: hard_break
---
