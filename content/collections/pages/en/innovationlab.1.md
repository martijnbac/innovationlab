---
id: 3838435d-88d2-4e06-8b88-c313b67f990e
origin: 367c0c11-b2fc-4ef0-a54d-501f320bfca3
title: 'InnovationLab EN'
description: 'Support for innovative education EN'
secondary_description: 'Innovative projects and international collaboration.'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651007633
---
