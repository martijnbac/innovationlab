---
id: 38daca41-0fc2-4419-b82a-6f58aedf14e2
origin: 1c348cfc-1962-40cf-a391-62dc03e19864
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651004263
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'EN – Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd.  Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd.'
---
