---
id: 3b8b8315-d098-4153-8d8c-34e7c173b8fa
blueprint: innotalks
title: 'Werken met learning outcomes'
image: 10-iPhone-11-Mockups.jpg
event_date: '2022-04-07'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1642434858
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd.  Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd.'
---
