---
id: 5cbfce2f-de10-434a-928c-9b374fb2992f
blueprint: innotalks
title: 'InnovationLab online platform voor beginners'
image: 'Academie-VO-&-MBO-44-b.jpg'
event_date: '2022-03-01 14:00'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1642434761
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd.  Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd.'
---
