---
id: 1c348cfc-1962-40cf-a391-62dc03e19864
blueprint: innotalks
title: 'Design based education workshop'
event_date: '2022-05-26 11:53'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1653565378
image: 'Academie-VO-&-MBO-38-b.jpg'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd.  Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd.'
---
