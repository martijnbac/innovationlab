---
id: ad80b3c0-f0a5-4d4a-8ae3-2eeb287e2da7
blueprint: onderwijs
title: iMinor
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651181579
image: 'Academie-VO-&-MBO-52-b.jpg'
excerpt: 'In een multidisciplinair team ontwerp en ontwikkel je – op basis van een authentieke opdracht uit de educatieve sector.'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'In een multidisciplinair team ontwerp en ontwikkel je – op basis van een authentieke opdracht uit de educatieve sector – een educatief multimediaal product. Je volgt hierbij de stappen van Design Based Education.'
---
