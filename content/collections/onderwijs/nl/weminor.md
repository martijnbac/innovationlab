---
id: ba8fc37a-b481-463e-bc83-7d7f83b30d18
blueprint: onderwijs
title: weMinor
template: innovationlab/show
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651181603
image: 'Academie-VO-&-MBO-38-b.jpg'
excerpt: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
---
