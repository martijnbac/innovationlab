---
id: b70f4ccc-8d91-41d5-86f6-e95c28f9b62d
blueprint: onderwijs
title: MySchoolsNetwork
image: 10-iPhone-11-Mockups.jpg
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651181592
excerpt: 'MySchoolsNetwork is a safe online, educational platform for pupils, students and teachers all over the world.'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'MySchoolsNetwork is a safe online, educational platform for pupils, students and teachers all over the world. The platform was founded to support digital literacy, global citizenship, authentic language learning and e-tutoring in primary, and secondary and higher education.'
---
