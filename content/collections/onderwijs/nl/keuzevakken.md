---
id: 9d7dd4c8-cad9-47b0-8273-153234c70e24
blueprint: onderwijs
title: Keuzevakken
image: 'Academie-VO-&-MBO-50-b.jpg'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651181584
excerpt: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
---
