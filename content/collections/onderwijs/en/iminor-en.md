---
id: bfd0dada-60a2-4e8d-be7a-bb59ff532df2
origin: ad80b3c0-f0a5-4d4a-8ae3-2eeb287e2da7
title: 'iMinor EN'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651178248
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'In een multidisciplinair team ontwerp en ontwikkel je – op basis van een authentieke opdracht uit de educatieve sector – een educatief multimediaal product. Je volgt hierbij de stappen van Design Based Education. EN'
---
