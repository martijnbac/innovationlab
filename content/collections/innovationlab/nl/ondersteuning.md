---
id: 27c5a6f9-ebd4-4f2f-8fc1-45eb49858789
blueprint: innovationlab
title: Ondersteuning
image: 'Academie-VO-&-MBO-44-b.jpg'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1654514641
excerpt: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo.'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd. '
  -
    type: heading
    attrs:
      level: 3
    content:
      -
        type: text
        text: 'Tussenkop h3'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd. Het InnovationLab team bevat expertise op het gebied van o.a. digitale didactiek, gamification, Universal Design for Learning, Design Thinking en het werken met Learning Outcomes.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd. Het InnovationLab team bevat expertise op het gebied van o.a. digitale didactiek, gamification, Universal Design for Learning, Design Thinking en het werken met Learning Outcomes.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd. Het InnovationLab team bevat expertise op het gebied van o.a. digitale didactiek, gamification, Universal Design for Learning, Design Thinking en het werken met Learning Outcomes.'
  -
    type: set
    attrs:
      values:
        type: quote
        quote: 'Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd. '
  -
    type: set
    attrs:
      values:
        type: video
        video: 'https://youtu.be/q2yGnxWBEDM'
  -
    type: paragraph
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab team is een ondersteunende unit binnen NHL Stenden Hogeschool en in het bijzonder Academie vo & mbo. Hier wordt innovatief onderwijs, met name ondersteund door een breed scala aan digitale middelen, ontworpen, uitgevoerd en geëvalueerd. Het InnovationLab team bevat expertise op het gebied van o.a. digitale didactiek, gamification, Universal Design for Learning, Design Thinking en het werken met Learning Outcomes.'
  -
    type: set
    attrs:
      values:
        type: carousel
        images:
          - 'Academie-VO-&-MBO-38-b.jpg'
          - 07-Magazine-Mockups-vol.jpg
          - 'Academie-VO-&-MBO-44-b.jpg'
          - Book-Cover-Mockups-02.jpg
          - 'Academie-VO-&-MBO-52-b.jpg'
  -
    type: paragraph
---
