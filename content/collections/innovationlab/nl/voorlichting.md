---
id: 460236cf-469c-4a2b-a221-e409cc7118fb
blueprint: innovationlab
title: Voorlichting
image: 'Academie-VO-&-MBO-52-b.jpg'
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651181561
excerpt: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
---
