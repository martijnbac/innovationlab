---
id: 06aa47ad-428f-4568-aa34-7d7f7bb19d58
blueprint: onderzoek
title: 'UDL Manual'
image: 07-Magazine-Mockups-vol.jpg
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651181615
excerpt: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
---
