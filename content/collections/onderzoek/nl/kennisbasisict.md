---
id: ce7edbc7-c68c-43db-9031-32c1adbaacd8
blueprint: onderzoek
title: KennisbasisICT
image: Book-Cover-Mockups-02.jpg
updated_by: cc2e518b-ccce-4d0f-aa2c-1b73f98fae09
updated_at: 1651181610
excerpt: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Het InnovationLab online platform ondersteunt innovatieve projecten en courses en stimuleert internationale samenwerking.'
---
